from crosszero import area, move, aii
from telegram import message


class game():

    def __init__(self,conf, logger,db, TGapi):
        self.logger = logger
        self.DB = db
        self.area = area()
        self.TG = TGapi
        self.message = message
        self.move = move
        self.ii = aii()

    def send_mes(self, i, TEXT):
        self.logger.info("Send messege " + TEXT + " To user " + str(i))
        message = self.message( text=TEXT, chat=i)
        self.TG.send_message(message.pre_send(self.logger))

    def send_area(self, i):
        self.logger.info("Send messege area  to user " + str(i.chat))
        message = self.message(text="", chat=i.chat)
        self.TG.send_message(message.pre_send(self.logger, self.area))

    def find_end(self, i, player, symb):
        self.logger.debug("Find end of game "+ str(i.chat))
        can_play = self.area.check_end()
        if can_play[0] == symb:
            self.logger.debug("Winner detect " + str(i.chat))
            self.send_mes(i.chat, "Gratz, " + player + " are winner!!")
            self.DB.set_game(self.area, i.chat)
            self.send_area(i)
            return False
        if can_play[1]:
            self.logger.info("Can move")
            return True
        else:
            self.logger.info("No move")
            return False # need message

    def game(self, i):
        if i.text == '/help':
            self.logger.info("Help " + str(i.chat))
            data = """
            *************************
            /help - bot command
            /new - Start new game
            /refresh - Refresh geame field
            /move_Y_X put you step on coordinat Y  and X position 
            *************************
            """
            self.send_mes(i.chat, data)

        if i.text == '/new':
            self.logger.info("Start new game with user " + str(i.chat))
            self.area = self.DB.new_game(i.chat)
            self.send_area(i)

        if i.text == '/refresh':
            self.logger.info("Refresh area for user " + str(i.chat) )
            self.area = self.DB.get_game(i.chat)
            if self.area is None:
                self.logger.info("Game with user " + str(i.chat) + " not found")
                self.send_mes(i.chat, "Not found game. You must write \\new for new game")
            else:
                can_play = self.area.check_end()
                if can_play[0] == 'X':
                    self.send_mes(i.chat, "Gratz, Player are winner!!")  # Congrat. Player win
                    self.send_area(i)
                elif can_play[0] == 'O':
                    self.send_mes(i.chat, "Gratz, II are winner!!")  # Congrat. Player ii
                    self.send_area(i)
                else:
                    self.send_area(i)

        if i.text.startswith('/move'):
            """Foramat move as /move_0_0"""
            x = int(i.text[6])-1
            y = int(i.text[8])-1
            self.area = self.DB.get_game(i.chat)
            self.logger.debug(self.area)
            self.logger.debug("Check correct coordinates " + str(i.chat))
            if x <0 or x > 2 or y <0 or y > 2:
                self.send_mes(i.chat,"Wrong move" )
            elif self.area is None:
                self.logger.debug("Not found started games for " + str(i.chat))
                self.send_mes(i.chat, "Not found game. You must write \\new for new game")
            else:
                if self.find_end(i, 'X', 'X') is False:
                    self.logger.debug("Check winner " + str(i.chat))
                    self.send_mes(i.chat, "Winner are X")
                else:
                    self.logger.debug("Check sector for step " + str(i.chat))
                    if self.area.zone(x, y) is False:
                        self.logger.debug("Sector are busy " + str(i.chat))
                        self.send_mes(i.chat, "Wrong move.Zone is busy")
                        self.send_area(i)
                    else:
                        """Do Player move"""
                        self.logger.debug("Player move correct. Do move " + str(i.chat))
                        m = self.move(x,y,'X')
                        self.area.step(m)
                        self.DB.set_game(self.area, i.chat)
                        """Check ii move"""
                        if self.find_end(i, 'X', 'X') is False: pass
                        else:
                            self.logger.debug("II step. Do move " + str(i.chat))
                            self.area = self.ii.step(self.area, 'O')
                            self.DB.set_game(self.area, i.chat)
                            self.send_area(i)
                            self.logger.debug("Check winner or empty areas " + str(i.chat))
                            if self.find_end(i, 'O', 'O') is False:
                                self.send_mes(i.chat,"Winner are O")

