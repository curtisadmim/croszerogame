import json
import requests


class telegramApi:

    def __init__(self, conf, logger, db):
        self.token = conf['token']
        self.logger = logger
        self.get_url = "https://api.telegram.org/bot%s/getUpdates" % self.token
        self.send_url = "https://api.telegram.org/bot%s/sendMessage" % self.token
        self.DB = db

    def get_message(self):
        self.logger.debug("Read new messages")
        offset = self.DB.check_mess()
        url = self.get_url + "?offset=" + str(offset)
        self.logger.debug("Calculate URL")
        self.logger.debug("get urls is " + url)
        try:
            self.logger.debug("Try to get massages")
            r = requests.get(url)
            mess = json.loads(r.text)
            try:
                self.logger.debug("Try to analyze")
                mess = mess['result']
                list = []
                for mes in mess:
                    try:
                        _ = mes['message']
                    except Exception as e:
                        self.logger.info(e)
                        break
                    list.append(message(message=mes))
                if len(mess) > 0:
                    self.logger.debug("Drop old message on server")
                    last = mess[len(mess) - 1]
                    self.DB.up_messsage(message_id=last['update_id'] + 1, time=last['message']['date'])
            except Exception as e:
                self.logger.error(e)
                self.logger.error(str(mess))
                return []
            self.logger.debug("Return massages")
            return list
        except requests.exceptions.ConnectionError as e:
            self.logger.error(e)
            return []


    def send_message(self, message):
        self.logger.debug("Send messages to " + str(message.chat))
        self.logger.debug(message.text)
        requests.post(self.send_url, json=message.text)

    def parse_message(self, m):
        self.logger.debug("Received " +  str(len(m)) + " raw messages")
        new_list = []
        self.logger.debug("Find service massages")
        self.logger.debug(str(m))
        for i in m:
            self.logger.debug(i.text)
            if i.text.startswith('/'):
                new_list.append(i)
        self.logger.debug("Received " + str(len(new_list)) + " sys messages")
        return new_list



class message:

    def __init__(self,  message = {}, text = '', chat = 0):
        if message != {}:
            self.mess = message
            self.text = self.mess['message']['text']
            self.time = self.mess['message']['date']
            self.chat = self.mess['message']['chat']['id']
            self.message_id = self.mess['message']['message_id']
            self.update = self.mess['update_id']
        else:
            self.text = text
            self.chat = chat

    def pre_send(self,logger, area= None):
        logger.debug("Create Payload befor sending to telegram")
        if area is not None:
            logger.debug("Have area. calculate area message for " + str(self.chat))
            list = area.get_area()
            text =  '___1___2___3__' + '\n'
            i = 1
            for line in list:
                text = text + str(i) +' |   '+ line[0] + ' |     ' + line[1] + ' |     ' + line[2]+ ' | ' + '\n'
                text = text + '________________' + '\n'
                i=i+1
            text = text + '___1___2___3__'
            self.text = {'text': text, 'chat_id': self.chat}
        else:
            logger.debug("Have raw text. create smple payload for " + str(self.chat))
            self.text = {'text': self.text, 'chat_id': self.chat}
        return self


