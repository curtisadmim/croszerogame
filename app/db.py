import sqlite3
from crosszero import area

class db:

    def __init__(self,logger=None,  conf=None, conn=None):
        self.logger = logger
        if self.logger is not None:
            self.logger.info("DB logger init")
        if conf is None and conn is None:
            if logger is not None:
                self.logger.info("Need link on coursor ore path for databesefile")
            raise Exception
        if conn is None:
            self.conn = sqlite3.connect(conf['db'])
        else: self.conn = conn
        self.cur = self.conn.cursor()
        if self.logger is not None:
            self.logger.info("Init tables")
        self.cur.execute("""CREATE TABLE IF NOT EXISTS users(
           userid INT PRIMARY KEY,
           fname TEXT,
           lname TEXT,
           username TEXT);
        """)
        self.conn.commit()
        self.cur.execute("""CREATE TABLE IF NOT EXISTS games(
                   userid INT PRIMARY KEY,
                   zPlayer TEXT,
                   crPlayer TEXT,
                   p0_0 TEXT,
                   p0_1 TEXT,
                   p0_2 TEXT,
                   p1_0 TEXT,
                   p1_1 TEXT,
                   p1_2 TEXT,
                   p2_0 TEXT,
                   p2_1 TEXT,
                   p2_2 TEXT);
                """)
        self.conn.commit()
        self.cur.execute("""CREATE TABLE IF NOT EXISTS mess(
           ID INT PRIMARY KEY,
           time INT);
        """)
        self.conn.commit()
        if self.logger is not None:
            self.logger.info("Init startup data")
        self.cur.execute("select * from mess")
        if len(self.cur.fetchall()) == 0:
            self.cur.execute("INSERT OR IGNORE into mess values(1, 1);")
        self.conn.commit()

    def get_game(self, user_id):
        if self.logger is not None:
            self.logger.info("Find game " + str(user_id))
        self.cur.execute("""select 
        p0_0, p0_1, p0_2, 
        p1_0, p1_1, p1_2, 
        p2_0, p2_1, p2_2 from games where userid = %s""" %user_id)
        rez = self.cur.fetchall()
        if len(rez) == 0:
            if self.logger is not None:
                self.logger.debug("Game " + str(user_id) + "not found")
            return None
        rez = rez[0]
        list = [[],[],[]]
        it = 0
        for i in range(3):
            for j in range(3):
                list[i].append(rez[it])
                it = it+1
        if self.logger is not None:
            self.logger.info("Find Game for " +str(user_id))
            self.logger.info(str(list))
        return area(list)



    def set_game(self, new_area, uid):
        if self.logger is not None:
            self.logger.debug("Set new game on BD " + uid)
        a = new_area.get_area()
        val = (a[0][0], a[0][1], a[0][2], a[1][0], a[1][1], a[1][2], a[2][0], a[2][1], a[2][2], uid)
        self.cur.execute("""UPDATE games SET
        p0_0 = '%s', p0_1 = '%s', p0_2 = '%s',
        p1_0 = '%s', p1_1 = '%s', p1_2 = '%s',
        p2_0 = '%s', p2_1 = '%s', p2_2 = '%s'
        where userid = %s""" %val)
        self.conn.commit()
        if self.logger is not None:
            self.logger.debug("Game seted " + uid)

    def find_user(self):
        pass

    def check_mess(self):
        if self.logger is not None:
            self.logger.info("Find OFFSET for api ")
        self.cur.execute("select ID from mess limit 1")
        rez = self.cur.fetchall()
        if len(rez) == 0:
            if self.logger is not None:
                self.logger.warning("Tables not init startup data")
            return 1
        if self.logger is not None:
            self.logger.debug("Last massege(OFFSET) " + str(rez))
        return rez[0][0]

    def up_messsage(self, message_id, time):
        if self.logger is not None:
            self.logger.debug("Update last read message " + str((message_id, time)))
        self.cur.execute("UPDATE mess SET ID = %s, time = %s where id is not null;" %(message_id,time))
        self.conn.commit()

    def new_game(self,  uid):
        if self.logger is not None:
            self.logger.debug("Init new game " + str(uid))
        if self.logger is not None:
            self.logger.debug("Find old game " + str(uid))
        self.cur.execute("select userid from games where userid = %s" %uid)
        rez = self.cur.fetchall()
        if len(rez)== 0:
            if self.logger is not None:
                self.logger.debug("Create new game " + str(uid))
            a = area().get_area()
            val = (uid, "","",a[0][0], a[0][1], a[0][2], a[1][0], a[1][1], a[1][2], a[2][0], a[2][1], a[2][2])
            self.cur.execute("insert into games values(%s, '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" %val)
            self.conn.commit()
            if self.logger is not None:
                self.logger.debug("New game created " + str(uid))
            return a
        else:
            if self.logger is not None:
                self.logger.debug("Game not found. Init new " + str(uid))
            a = area()
            self.set_game(a,uid)
            return a