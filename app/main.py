import json
import logging
import logging.config
import pathlib
from  db import db
from telegram import telegramApi
import time
from processor import game



def log():
    pathlib.Path('./log').mkdir(parents=True, exist_ok=True)
    log_file_path = pathlib.Path("./log/result.log")
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler(log_file_path),
            logging.StreamHandler()
                ])
    return logging.getLogger()


def config():
    f = open("conf.json")
    return json.loads(f.read())


def main():
    logger = log()
    logger.info("Read conf")
    try:
        conf = config()
    except Exception as e:
        logger.error(e)
        logger.error("Cant read log")
        raise Exception
    logger.info("Conf is read")
    logger.info("Create DB connect")
    DB = db(conf=conf)
    logger.info("DB are ready")
    TG = telegramApi(conf, logger,DB)
    g = game(conf, logger,DB, TG)
    logger.info("Program started")
    logger.info("Start listening")

    while True:
        logger.debug("Program sleap " + str(conf['timeout']) + " seconds")
        time.sleep(int(conf['timeout']))
        logger.debug("Program stop  sleaping ")
        m = TG.parse_message(TG.get_message())
        logger.debug("Received " +  str(len(m)) + " messages")
        for i in m:
            g.game(i)


if __name__ == "__main__":
    main()
