import sqlite3
import sys
import pytest
sys.path.insert(0, '../')
from crosszero import area
from crosszero import move
from crosszero import aii
from db import db



def test_init_get_area():
    a = area()
    assert a.get_area() == [['', '', ''], ['', '',''], ['', '', '']]

def test_zone_pos():
    a = area([['X', '', ''], ['', '',''], ['', '', '']])
    assert  a.zone(0,1)
    assert a.zone(1, 1)
    assert a.zone(0, 0) is False

def test_zone_another_pos():
    a = area([['X', '', ''], ['', '',''], ['', '', '']])
    assert a.zone(1, 1)

def test_zone_bysy_cross():
    a = area([['X', '', ''], ['', '',''], ['', '', '']])
    assert a.zone(0, 0) is False

def test_zone_bysy_zero():
    a = area([['O', '', ''], ['', '',''], ['', '', '']])
    assert a.zone(0, 0) is False

def test_step_cross():
    a = area([['', '', ''], ['', '',''], ['', '', '']])
    m = move(0,0,'X')
    a.step(m)
    assert a.get_area() == [['X', '', ''], ['', '',''], ['', '', '']]

def test_step_zero():
    a = area([['', '', ''], ['', '',''], ['', '', '']])
    m = move(0,2,'O')
    a.step(m)
    assert a.get_area() == [['', '', 'O'], ['', '',''], ['', '', '']]

def test_step_zero_2():
    a = area([['', '', ''], ['', '',''], ['', '', '']])
    m = move(2,1,'O')
    a.step(m)
    assert a.get_area() == [['', '', ''], ['', '',''], ['', 'O', '']]

def test_end_1():
    a = area([['O', 'O', 'O'],
              ['', '',''],
              ['', '', '']])
    assert a.check_end() == ('O', False)

def test_end_2():
    a = area([['O', '', ''],
              ['X', 'X','X'],
              ['', 'O', 'O']])
    assert a.check_end() == ('X', False)

def test_end_3():
    a = area([['O', '', ''],
              ['X', 'O','X'],
              ['', 'O', 'O']])
    assert a.check_end() == ('O', False)

def test_end_4():
    a = area([['X', '', 'O'],
              ['X', 'O','X'],
              ['O', '', '']])
    assert a.check_end() == ('O', False)

def test_end_5():
    a = area([['X', 'X', 'O'],
              ['O', 'X','X'],
              ['O', 'X', 'O']])
    assert a.check_end() == ('X', False)

def test_end_6():
    a = area([['O', 'X', 'O'],
              ['O', 'O','X'],
              ['O', 'X', 'O']])
    assert a.check_end() == ('O', False)

def test_end_7():
    a = area([['O', 'X', 'X'],
              ['X', 'O','O'],
              ['O', 'X', 'X']])
    assert a.check_end() == ('', False)

def test_end_8():
    a = area([['O', 'X', 'X'],
              ['X', '','O'],
              ['O', 'X', 'X']])
    assert a.check_end() == ('', True)

def test_aii_1():
    a = area([['O', 'X', 'X'],
              ['X', '','O'],
              ['O', 'X', 'X']])
    bot = aii()
    b = area([['O', 'X', 'X'],
              ['X', 'X','O'],
              ['O', 'X', 'X']])
    assert bot.step(a,"X").get_area() == b.get_area()

def test_aii_2():
    a = area([['O', '', 'X'],
              ['X', '','O'],
              ['O', 'X', 'X']])
    bot = aii()
    b = area([['O', 'O', 'X'],
              ['X', '','O'],
              ['O', 'X', 'X']])
    assert bot.step(a,"O").get_area() == b.get_area()

@pytest.fixture
def get_connect():
    conn = sqlite3.connect(":memory:")
    return conn

def test_db_tables(get_connect):
    db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("select name from sqlite_master where type = 'table';")
    tables = cur.fetchall()
    assert tables == [('users',), ('games',), ('mess',)]

def test_db_games_table_info(get_connect):
    db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("PRAGMA table_info(games)")
    tables = cur.fetchall()
    assert tables == [(0, 'userid', 'INT', 0, None, 1),
                      (1, 'zPlayer', 'TEXT', 0, None, 0),
                      (2, 'crPlayer', 'TEXT', 0, None, 0),
                      (3, 'p0_0', 'TEXT', 0, None, 0),
                      (4, 'p0_1', 'TEXT', 0, None, 0),
                      (5, 'p0_2', 'TEXT', 0, None, 0),
                      (6, 'p1_0', 'TEXT', 0, None, 0),
                      (7, 'p1_1', 'TEXT', 0, None, 0),
                      (8, 'p1_2', 'TEXT', 0, None, 0),
                      (9, 'p2_0', 'TEXT', 0, None, 0),
                      (10, 'p2_1', 'TEXT', 0, None, 0),
                      (11, 'p2_2', 'TEXT', 0, None, 0)]

def test_db_mess_table_info(get_connect):
    db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("PRAGMA table_info(mess)")
    tables = cur.fetchall()
    assert tables == [(0, 'ID', 'INT', 0, None, 1), (1, 'time', 'INT', 0, None, 0)]

def test_db_mess_default_data(get_connect):
    db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("select * from mess")
    lines = cur.fetchall()
    assert lines == [(1, 1)]


def test_db_get_game(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("INSERT INTO games VALUES(123654, 0, 0, '', '', 'O', '', '', 'O', '', '', '');")
    cur.execute("INSERT INTO games VALUES(123656, 0, 0, 'X', 'X', '', 'O', '', 'O', '', 'X', '');")
    cur.execute("INSERT INTO games VALUES(123657, 0, 0, '', '', '', '', '', '', '', '', 'X');")
    get_connect.commit()
    game = DB.get_game(123656)
    a = area([['X', 'X', ''],
              ['O', '','O'],
              ['', 'X', '']])
    assert game.get_area() == a.get_area()

def test_db_set_game(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("INSERT INTO games VALUES(123654, 0, 0, '', '', 'O', '', '', 'O', '', '', '');")
    cur.execute("INSERT INTO games VALUES(123656, 0, 0, 'X', 'X', '', 'O', '', 'O', '', 'X', '');")
    cur.execute("INSERT INTO games VALUES(123657, 0, 0, '', '', '', '', '', '', '', '', 'X');")
    get_connect.commit()
    a = area([['X', 'X', ''],
              ['O', '','O'],
              ['', 'X', '']])
    DB.set_game(a, 123657)
    cur.execute("select * from games where userid = %s" %(123657))
    game = cur.fetchall()
    assert game == [(123657, '0', '0', 'X', 'X', '', 'O', '', 'O', '', 'X', '')]

def test_db_up_mess(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    DB.up_messsage(625, 15665445)
    cur.execute("select * from mess")
    last  = cur.fetchall()
    assert last == [(625,15665445)]
    DB.up_messsage(627, 15665455)
    cur.execute("select * from mess")
    last  = cur.fetchall()
    assert last == [(627, 15665455)]

def test_db_check_mess(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    DB.up_messsage(256, 15665445)
    last  = DB.check_mess()
    assert last == 256
    cur.execute("UPDATE mess SET ID = %s, time = %s;" %(856,15665499))
    get_connect.commit()
    last = DB.check_mess()
    assert last == 856

def test_db_new_game_1(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("INSERT INTO games VALUES(123654, 0, 0, '', '', 'O', '', '', 'O', '', '', '');")
    cur.execute("INSERT INTO games VALUES(123656, 0, 0, 'X', 'X', '', 'O', '', 'O', '', 'X', '');")
    cur.execute("INSERT INTO games VALUES(123657, 0, 0, '', '', '', '', '', '', '', '', 'X');")
    get_connect.commit()
    DB.new_game(123656)
    cur.execute("select * from games where userid = %s" %(123656))
    game = cur.fetchall()
    assert game == [(123656, '0', '0', '', '', '', '', '', '', '', '', '')]

def test_db_new_game_2(get_connect):
    DB = db(conn=get_connect)
    cur = get_connect.cursor()
    cur.execute("INSERT INTO games VALUES(123654, 0, 0, '', '', 'O', '', '', 'O', '', '', '');")
    cur.execute("INSERT INTO games VALUES(123656, 0, 0, 'X', 'X', '', 'O', '', 'O', '', 'X', '');")
    cur.execute("INSERT INTO games VALUES(123657, 0, 0, '', '', '', '', '', '', '', '', 'X');")
    get_connect.commit()
    G = DB.new_game(123658)
    cur.execute("select * from games where userid = %s" %(123656))
    game = cur.fetchall()
    assert game == [(123656, '0', '0', 'X', 'X', '', 'O', '', 'O', '', 'X', '')]
    cur.execute("select * from games where userid = %s" % (123658))
    game = cur.fetchall()
    assert game == [(123658, '', '', '', '', '', '', '', '', '', '', '')]
    a = area()
    assert a.get_area() == G



